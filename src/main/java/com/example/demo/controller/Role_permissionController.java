package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.services.Role_permissionService;

import net.sf.json.JSONArray;

@RestController
@RequestMapping("/role_permission")
public class Role_permissionController {
	
	@Autowired
	private Role_permissionService role_permissionService;
		
	
	/**
	 * 	根据角色ID查询所有此角色拥有的权限ID
	 * @param roleId
	 * @return
	 */
	@RequestMapping("/getPermissionIdByRoleId")
	@ResponseBody
	public Object getPermissionIdByRoleId(int role_id) {
		System.out.println();
		Map<String, Object> obj = new HashMap<String, Object>();
		List<Integer> permissionId = role_permissionService.getPermissionIdByRoleId(role_id);
		obj.put("data", permissionId);
		return obj;
	}
	
	
	@RequestMapping("/savePermissionByRoleId")
	public String savePermissionByRoleId(String menuId, String moduleId, String role_id, String data) {
		String result = "0";
		Map<String,Object> conditions = new HashMap<String,Object>();
		List permission = new ArrayList();
		JSONArray permissionData = JSONArray.fromObject(data);
		for(int i = 0; i < permissionData.length(); i++) {
			permission.add(permissionData.get(i));
		}
		conditions.put("menuId", menuId);
		conditions.put("moduleId", moduleId);
		conditions.put("permission", permission);
		conditions.put("role_id", role_id);	
		System.out.println(conditions);
			result = role_permissionService.savePermissionByRoleId(conditions) + "";

		return result;
	}

}
