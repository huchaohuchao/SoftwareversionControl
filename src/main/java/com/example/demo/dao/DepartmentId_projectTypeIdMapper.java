package com.example.demo.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.example.demo.domain.DepartmentId_projectTypeId;

@Mapper
public interface DepartmentId_projectTypeIdMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DepartmentId_projectTypeId record);

    int insertSelective(DepartmentId_projectTypeId record);

    DepartmentId_projectTypeId selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DepartmentId_projectTypeId record);

    int updateByPrimaryKey(DepartmentId_projectTypeId record);
    
    List<Map<String,Object>> projectTypeSelectionByDepartmentId(@Param("departmentId")Integer departmentId);
}