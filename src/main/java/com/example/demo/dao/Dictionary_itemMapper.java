package com.example.demo.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.example.demo.domain.Dictionary_item;

@Mapper
public interface Dictionary_itemMapper {

	//查询数据字典
	public Dictionary_item queryDictionary(String type);
	
	//查询数据字典总数
	int queryDictionaryCount(Map map);
		
	//查询数据字典列表
	List<Dictionary_item> queryDictionaryList(Map<String, Object> map);	
		
    int deleteByPrimaryKey(int id);

    int insertSelective(Dictionary_item record);

    int updateByPrimaryKeySelective(Dictionary_item record);
}