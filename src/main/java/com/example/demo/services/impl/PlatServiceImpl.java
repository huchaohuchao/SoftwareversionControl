package com.example.demo.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.PlatMapper;
import com.example.demo.dao.ProcessinstanceFileMapper;
import com.example.demo.domain.Plat;
import com.example.demo.domain.ProcessinstanceFile;
import com.example.demo.domain.Project_details;
import com.example.demo.services.PlatService;

@Service
public class PlatServiceImpl implements PlatService {
	
	@Autowired
	private PlatMapper platMapper;
	
	@Autowired
	private ProcessinstanceFileMapper processinstanceFileMapper;

	@Override
	public List<Plat> findPlatByConditions(Map<String, String> condtions) {
		List<Plat> platList = platMapper.selectPlatByCondtions(condtions);
		return platList;
	}

	@Override
	public void addNewPlat(String platName, String platNumber) {
		Plat record = new Plat();
		record.setName(platName);;
		record.setNumber(platNumber);
		platMapper.insertSelective(record);		
	}

	@Override
	public List<Map<String, Object>> selectProcessInstanceByPlatId(String platId) {
		List<Map<String, Object>> result = platMapper.selectProcessInstanceByPlatId(platId);
		return result;
	}

	@Override
	public List<ProcessinstanceFile> findPlatProcessInstanceFileTableByPid(String processInstanceId) {
		List<ProcessinstanceFile> list = processinstanceFileMapper.selectProcessinstanceFileByPid(processInstanceId);
		return list;
	}

	@Override
	public int deletePlatById(String platId) {
		int result = platMapper.deleteByPrimaryKey(Integer.parseInt(platId));
		return result;
	}

	@Override
	public List<Map<String, Object>> selectPlatProcessInstanceByPid(String processInstanceId) {
		List<Map<String, Object>> project_details = platMapper.selectPlatProcessInstanceByProjectPid(processInstanceId); 
		return project_details;
	}

}
