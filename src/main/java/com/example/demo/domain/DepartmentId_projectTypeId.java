package com.example.demo.domain;

public class DepartmentId_projectTypeId {
    private Integer id;

    private Integer departmentid;

    private Integer projecttypeid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDepartmentid() {
        return departmentid;
    }

    public void setDepartmentid(Integer departmentid) {
        this.departmentid = departmentid;
    }

    public Integer getProjecttypeid() {
        return projecttypeid;
    }

    public void setProjecttypeid(Integer projecttypeid) {
        this.projecttypeid = projecttypeid;
    }
}