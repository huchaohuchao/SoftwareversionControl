package com.example.demo.controller;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.PageBean;
import com.example.demo.domain.Sysuser;
import com.example.demo.services.SysuserService;
import com.example.demo.util.Util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@RestController
@RequestMapping(value = "/sysuser")
public class SysuserController{
	
	private static Logger logger = Logger.getLogger(SysuserController.class);
	
	@Autowired
	private SysuserService sysuserService;
	
   /**
    *	查询用户列表
    * @author hc
	* @param request
	* @param response
	* @throws Exception
    */
    @RequestMapping(value = "/userList")
    public String sysuerList(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	Map<String, Object> map = new HashMap();
    	JSONObject obj=new JSONObject();
    	try{
    		map.put("name", request.getParameter("name"));
    		map.put("username", request.getParameter("username"));
    		map.put("department", request.getParameter("department"));
    		map.put("role_id", request.getParameter("role_id"));
    		map.put("pageNum", Integer.parseInt(request.getParameter("page")));
    		map.put("pageSize", Integer.parseInt(request.getParameter("limit")));
    		PageBean<Sysuser> pb = sysuserService.queryUserList(map);
    		if(pb.getList().size() < 0){
    			obj.put(Util.RESULT, Util.RESULT_RC_CHECK_FAIL);
    			obj.put(Util.MESSAGE, "查询用户列表失败");
    		}else{
    			obj.put("data", pb.getList());
    			obj.put(Util.COUNT, pb.getTotalRecord());
    			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
    			obj.put(Util.MESSAGE, "查询用户列表成功！");
    		}       		
		} catch (Exception e) {
			logger.error("=============[SysuserController[userList[查询用户列表失败]]]==========",e);
		}
    	return obj.toString();
    }
    
    /**
     *	新增或编辑用户
     * @author hc
 	 * @param request
 	 * @param response
 	 * @throws Exception
     */
     @RequestMapping(value = "/userAdd")
     public String userAdd(HttpServletRequest request, HttpServletResponse response) throws Exception{
     	JSONObject obj=new JSONObject();
     	try{
     		Sysuser sysuser = new Sysuser();
     		sysuser.setName(request.getParameter("sysname"));
     		sysuser.setPassword(DigestUtils.md5DigestAsHex(request.getParameter("password").getBytes()));
     		sysuser.setUsername(request.getParameter("username"));
     		if(request.getParameter("roleSelect")!=null && !"".equals(request.getParameter("roleSelect"))) {
     			sysuser.setRole_id(Integer.parseInt(request.getParameter("roleSelect")));
     		}
     		if(request.getParameter("departmentSelect")!=null && !"".equals(request.getParameter("departmentSelect"))) {
     			sysuser.setDepartment(request.getParameter("departmentSelect"));
     		}
     		if(request.getParameter("is_delete")!=null && !"".equals(request.getParameter("is_delete"))) {
     			sysuser.setIs_delete(Integer.parseInt(request.getParameter("is_delete")));
     		}
     		if(request.getParameter("positionId")!=null && !"".equals(request.getParameter("positionId"))) {
     			sysuser.setPositionId(request.getParameter("positionId"));
     		}
     		if(request.getParameter("id") == null || "".equals(request.getParameter("id"))) {
     			sysuserService.userAdd(sysuser);
     			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
     			obj.put(Util.MESSAGE, "新增用户成功！");
     		} else {
     			sysuser.setId(Integer.parseInt(request.getParameter("id")));
     			sysuserService.userUpdate(sysuser);
     			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
     			obj.put(Util.MESSAGE, "编辑用户成功！");
     		}     		
 		} catch (Exception e) {
 			logger.error("=============[SysuserController[userAdd[新增或编辑用户失败]]]==========",e);
 		}
     	return obj.toString();
     }
 
     /**
      *	软删除用户
      * @author hc
  	  * @param request
      * @param response
  	  * @throws Exception
      */
      @RequestMapping(value = "/userDelete")
      public String userDelete(HttpServletRequest request, HttpServletResponse response) throws Exception{
      	JSONObject obj=new JSONObject();
      	try{
      		if(request.getParameter("id") != null && !"".equals(request.getParameter("id"))) {
      			sysuserService.userDelete(Integer.parseInt(request.getParameter("id")));
      			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
      			obj.put(Util.MESSAGE, "删除用户成功！");
      		}	
  		} catch (Exception e) {
  			logger.error("=============[SysuserController[userDelete[删除用户失败]]]==========",e);
  		}
      	return obj.toString();
      }
      
      /**
       *	软删除用户
       * @author dsh
   	  * @param request
       */
       @RequestMapping(value = "/findUserRole")
       @ResponseBody
       public Object findUserRole(HttpServletRequest request, HttpServletResponse response) throws Exception{
       	try{
       		Sysuser role = sysuserService.findUserRole();
       		return role;
   		} catch (Exception e) {
   		    return "";
   			
   		}
       }
      
      
}