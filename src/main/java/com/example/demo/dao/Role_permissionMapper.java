package com.example.demo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.example.demo.domain.Role_permission;

@Mapper
public interface Role_permissionMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table role_permission
	 * @mbg.generated
	 */
	int deleteByPrimaryKey(Integer id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table role_permission
	 * @mbg.generated
	 */
	int insert(Role_permission record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table role_permission
	 * @mbg.generated
	 */
	int insertSelective(Role_permission record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table role_permission
	 * @mbg.generated
	 */
	Role_permission selectByPrimaryKey(Integer id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table role_permission
	 * @mbg.generated
	 */
	int updateByPrimaryKeySelective(Role_permission record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table role_permission
	 * @mbg.generated
	 */
	int updateByPrimaryKey(Role_permission record);
	
	
	/**
	 * 	根据角色ID查询所有此角色拥有的权限ID
	 * @param roleId
	 * @return
	 */
	public List<Integer> queryPermissionIdByRoleId(int roleId); 
	
	//根据角色删除此角色的权限
	int deletePermissionByRoleId(int role_id);
	
	//根据1级菜单删除此角色的权限
    int deletePermissionByMenuId(@Param("menuId") int menuId, @Param("role_id") int role_id);
	
    //根据2级模块删除此角色的权限
	int deletePermissionByModuleId(@Param("moduleId") int moduleId, @Param("role_id") int role_id);
	
}