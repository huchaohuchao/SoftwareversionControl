package com.example.demo.services.impl;

import java.util.List;
import java.util.Map;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.SysuserMapper;
import com.example.demo.domain.PageBean;
import com.example.demo.domain.Sysuser;
import com.example.demo.services.SysuserService;
import com.github.pagehelper.PageHelper;

@Service
@Transactional
public class SysuserServiceImpl implements SysuserService {
	@Autowired
	private SysuserMapper sysuserMapper;

	/**
	 *	查询用户列表
	 * @author hc
	 * @param
	 * @return List<Sysuser>
	 */
	public PageBean<Map<String, Object>> queryUserList(Map<String, Object> map){
		PageHelper.startPage((Integer)map.get("pageNum"), (Integer)map.get("pageSize")); 
		List<Map<String, Object>> user = sysuserMapper.queryUserList(map);
		//获取起始记录条数
		PageBean pb = new PageBean((Integer)map.get("pageNum"), (Integer)map.get("pageSize"), sysuserMapper.queryUserCount(map));
		pb.setList(user);
    	return pb;
    }	
	
	/**
	 *	新增用户
	 * @author hc
	 * @param Sysuser
	 * @return
	 */
	public void userAdd(Sysuser sysuser) {
		sysuserMapper.insertSelective(sysuser);
	}
	
	/**
	 *	编辑用户
	 * @author hc
	 * @param Sysuser
	 * @return 
	 */
	public void userUpdate(Sysuser sysuser) {
		sysuserMapper.updateByPrimaryKeySelective(sysuser);
	}
	
	/**
	 *	删除用户
	 * @author hc
	 * @param id
	 * @return 
	 */
	public void userDelete(int id) {
		sysuserMapper.userDelete(id);
	}

	@Override
	public Sysuser  findUserRole() {
		//从会话中取出用户信息
    	Object username = SecurityUtils.getSubject().getPrincipal();
    	//查询用户名称
        Sysuser sysuser = sysuserMapper.findByName(username.toString());
		return sysuser;
	}
}
