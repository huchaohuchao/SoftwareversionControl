package com.example.demo.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.example.demo.domain.Sysuser;

@Mapper
public interface SysuserMapper {
	//根据部门id查询部门人员
	List<Sysuser> departmentStaffSelection(@Param("department")String department_id,@Param("isNextApprovel")String isNextApprovel);
	
	//根据用户id查询用户姓名
	String findById(String id);
	
	//查询用户总数
	int queryUserCount(Map map);
	
	//软删除用户
	void userDelete(int id);
	
	//查询用户列表
	List<Map<String, Object>> queryUserList(Map<String, Object> map);
	
	//根据用户名查询用户信息
	Sysuser findByName(String username);

	
	int deleteByPrimaryKey(Integer id);

	int insertSelective(Sysuser record);

	int updateByPrimaryKeySelective(Sysuser record);
	
	int saveRolePermission(Map map);
	
	int selectMinister(String department);

}