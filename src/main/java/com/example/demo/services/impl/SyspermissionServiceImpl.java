package com.example.demo.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.Sys_permissionMapper;
import com.example.demo.domain.Sys_permission;
import com.example.demo.services.SyspermissionService;

@Service
@Transactional
public class SyspermissionServiceImpl implements SyspermissionService {
	
	@Autowired
	private Sys_permissionMapper syspermissionMapper;
	
	

	/**
	 * 根据查询条件返回权限表单
	 * @param map
	 * @return
	 */
	public List<Sys_permission> querySyspermissionTable(Map<String, String> map) {
		List<Sys_permission> result = syspermissionMapper.querySyspermissionByConditions(map);
		return result;
	}


	/**
	 *       查询所有的1.2级的syspermission到前台做成select
	 * @param map
	 * @return
	 */
	public List<Sys_permission> querySyspermissionSelect() {
		List<Sys_permission> data = syspermissionMapper.querySyspermissionSelect();
		return data;
	}


	
	/**
	 *       查询所有父id为menuId的权限对象 
	 * @param map
	 * @return
	 */
	@Override
	public List<Sys_permission> querySyspermissionByMenuId(Map map) {
		List<Sys_permission> data = syspermissionMapper.querySyspermissionByMenuId(map);
		return data;
	}


	
	@Override
	public int saveSyspermission(Sys_permission sys_permission) {
		int result = syspermissionMapper.insertSelective(sys_permission);
		return result;
	}


	@Override
	public int updateSyspermission(Sys_permission sys_permission) {
		int result = syspermissionMapper.updateByPrimaryKey(sys_permission);
		return result;
	}


	@Override
	public int deleteSyspermissionById(int id) {
		int result = syspermissionMapper.deleteByPrimaryKey(id);
		return result;
	}

}
