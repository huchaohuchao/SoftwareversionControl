package com.example.demo.services;

import java.util.List;
import java.util.Map;

import com.example.demo.domain.Sys_permission;

public interface LoginService {
	    
    //查询用户主页模块数据
    List<Sys_permission> queryLeftModel(String username);
}
