package com.example.demo.domain;

public class Sys_permission {

	private Integer id;

	private String title;

	private Integer type;

	private String href;
	
	private Integer parent_id;

	private String spread;

	public Integer getParent_id() {
		return parent_id;
	}

	public void setParent_id(Integer parent_id) {
		this.parent_id = parent_id;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title == null ? null : title.trim();
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href == null ? null : href.trim();
	}

	public String getSpread() {
		return spread;
	}

	public void setSpread(String spread) {
		this.spread = spread == null ? null : spread.trim();
	}

	@Override
	public String toString() {
		return "Sys_permission [id=" + id + ", title=" + title + ", type=" + type + ", href=" + href + ", parent_id="
				+ parent_id + ", spread=" + spread + "]";
	}

}