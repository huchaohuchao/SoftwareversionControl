package com.example.demo.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.Sys_permissionMapper;
import com.example.demo.domain.Sys_permission;
import com.example.demo.services.LoginService;

@Service
@Transactional
public class LoginServiceImpl implements LoginService {
    @Autowired
    private Sys_permissionMapper sys_permissionMapper;
    
    public List<Sys_permission> queryLeftModel(String username){
    		return sys_permissionMapper.queryLeftModel(username);

    }
}