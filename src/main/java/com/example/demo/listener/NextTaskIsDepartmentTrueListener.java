package com.example.demo.listener;

import org.activiti.engine.delegate.DelegateExecution; 
import org.activiti.engine.delegate.ExecutionListener;

public class NextTaskIsDepartmentTrueListener implements ExecutionListener{
	
	private static final long serialVersionUID = 1L;

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		System.out.println("经过了===================================================================");
		execution.setVariable("isDepartment", true);//true代表是下一节点是部门,false代表不是		
	}

}
