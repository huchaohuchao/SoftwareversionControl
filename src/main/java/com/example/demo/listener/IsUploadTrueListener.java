package com.example.demo.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

public class IsUploadTrueListener implements ExecutionListener{
	
	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// TODO Auto-generated method stub
		execution.setVariable("isUpload", true);//true代表可以上传程序或者标定文件,false代表不能
	}

}
