package com.example.demo.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.example.demo.domain.ProjectId_processInstanceId;

@Mapper
public interface ProjectId_processInstanceIdMapper {
	//查询正在运行的任务列表
	List<Map<String, Object>> queryRuntimeList(Map<String, Object> map);
	
	//查询正在运行的任务总数
	int queryRuntimeListCount(Map<String, Object> map);
	
	//查询业务流程列表
	List<Map<String, Object>> queryBusinessProcessList(Map<String, Object> map);
	
	//查询业务流程列表总数
	int queryBusinessProcessListCount(Map<String, Object> map);
		
    int deleteByPrimaryKey(String processinstanceid);

    int insert(ProjectId_processInstanceId record);

    int insertSelective(ProjectId_processInstanceId record);

    ProjectId_processInstanceId selectByPrimaryKey(String processinstanceid);

    int updateByPrimaryKeySelective(ProjectId_processInstanceId record);

    int updateByPrimaryKey(ProjectId_processInstanceId record);
}