package com.example.demo.domain;

import java.util.Date;

public class ProcessinstanceFileHistory {
    private Integer id;

	private String filename;

	private String url;

	private Date time;

	private String fileremarks;

	private Integer userid;

	private Integer processinstancefileid;

	private String taskid;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFilename() {
		return filename;
	}

	@Override
	public String toString() {
		return "ProcessinstanceFileHistory [id=" + id + ", filename=" + filename + ", url=" + url + ", time=" + time
				+ ", fileremarks=" + fileremarks + ", userid=" + userid + ", processinstancefileid="
				+ processinstancefileid + ", taskid=" + taskid + "]";
	}

	public void setFilename(String filename) {
		this.filename = filename == null ? null : filename.trim();
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url == null ? null : url.trim();
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getFileremarks() {
		return fileremarks;
	}

	public void setFileremarks(String fileremarks) {
		this.fileremarks = fileremarks == null ? null : fileremarks.trim();
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public Integer getProcessinstancefileid() {
		return processinstancefileid;
	}

	public void setProcessinstancefileid(Integer processinstancefileid) {
		this.processinstancefileid = processinstancefileid;
	}

	public String getTaskid() {
		return taskid;
	}

	public void setTaskid(String taskid) {
		this.taskid = taskid == null ? null : taskid.trim();
	}

}