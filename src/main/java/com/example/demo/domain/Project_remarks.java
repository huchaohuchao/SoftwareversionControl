package com.example.demo.domain;

public class Project_remarks {
    private Integer id;

    private Integer user_id;

    private String processInstance_id;

    private String remarks;

    private Integer task_id;

    private String node_name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getProcessInstance_id() {
        return processInstance_id;
    }

    public void setProcessInstance_id(String processInstance_id) {
        this.processInstance_id = processInstance_id == null ? null : processInstance_id.trim();
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    public Integer getTask_id() {
        return task_id;
    }

    public void setTask_id(Integer task_id) {
        this.task_id = task_id;
    }

    public String getNode_name() {
        return node_name;
    }

    public void setNode_name(String node_name) {
        this.node_name = node_name == null ? null : node_name.trim();
    }

	@Override
	public String toString() {
		return "Project_remarks [id=" + id + ", user_id=" + user_id + ", processInstance_id=" + processInstance_id
				+ ", remarks=" + remarks + ", task_id=" + task_id + ", node_name=" + node_name + "]";
	}
    
}