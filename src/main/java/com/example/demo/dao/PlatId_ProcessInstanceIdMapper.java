package com.example.demo.dao;

import org.apache.ibatis.annotations.Mapper;

import com.example.demo.domain.PlatId_ProcessInstanceId;

@Mapper
public interface PlatId_ProcessInstanceIdMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PlatId_ProcessInstanceId record);

    int insertSelective(PlatId_ProcessInstanceId record);

    PlatId_ProcessInstanceId selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PlatId_ProcessInstanceId record);

    int updateByPrimaryKey(PlatId_ProcessInstanceId record);
    
    PlatId_ProcessInstanceId selectByProcessInstanceId(String processInstanceId);
}