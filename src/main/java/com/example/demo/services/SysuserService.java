package com.example.demo.services;

import java.util.List;
import java.util.Map;

import com.example.demo.domain.PageBean;
import com.example.demo.domain.Sysuser;


public interface SysuserService {
	
	/**
	 *	查询用户列表
	 * @author hc
	 * @param 
	 * @return List<Sysuser>
	 */
	PageBean queryUserList(Map<String, Object> map);
	
	/**
	 *	新增用户
	 * @author hc
	 * @param Sysuser
	 * @return
	 */
	void userAdd(Sysuser sysuser);
	
	/**
	 *	编辑用户
	 * @author hc
	 * @param Sysuser
	 * @return 
	 */
	void userUpdate(Sysuser sysuser);
	
	/**
	 *	删除用户
	 * @author hc
	 * @param id
	 * @return 
	 */
	void userDelete(int id);
	
	public Sysuser  findUserRole();
}
