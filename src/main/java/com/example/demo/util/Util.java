package com.example.demo.util;

import java.text.ParseException; 
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;



/**
 * ClassName: Util
 * 
 * @Description: 工具类
 * @author yewl
 * @date 2015年11月16日
 */
public class Util {
	private static Logger logger = Logger.getLogger(Util.class);

	/**接口返回信息设置*/
	public static final String RESULT="code";//返回状态码
	
	public static final String MESSAGE="msg";//返回消息
	
	public static final String COUNT="count";//返回总数
	/**正常返回*/
	public static final String RESULT_RC_SUCCESS = "0";
	
	/**正常返回-- 自己激活的设备*/
	public static final String RESULT_SL_SUCCESS = "0001";
	
	/** 正常返回没有查到数据 */
	public static final String RESULT_RC_NO_DATA = "1111";
	
	/** 系统异常 */
	public static final String RESULT_RC_EXCEPTION = "-9999";
	
	/**校验失败*/
	public static final String RESULT_RC_CHECK_FAIL = "0002";

	
	
	/**
	 * 判断两个对象是否相等
	 * 
	 * @param o1
	 * @param o2
	 * @return
	 */
	public static boolean equal(Object o1, Object o2) {
		if (o1 == o2) {
			return true;
		}
		if (o1 != null && o1.equals(o2)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 把date类型的时间转换成String格式的年月日时分秒
	 * 
	 * @param Date date
	 * @return String
	 */
	public static String currentTime(Date date) {
		java.text.DateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String s = format.format(date);
		return s;
	}
	
	/**
	 * 把yyyy-MM-dd的字符串转换成date类型
	 * 
	 * @param String
	 * @return Date
	 * @throws ParseException 
	 */
	public static Date dateStrToDate(String dateStr) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			date = new Date();
		}
		return date;
	}
	
	 /**
     * 十六进制转字符串
     * 
     * @param hexString
     *            十六进制字符串
     * @param encodeType
     *            编码类型4：Unicode，2：普通编码
     * @return 字符串
     */
	public static String hexStringToString(String hexString, int encodeType) {
        String result = "";
        int max = hexString.length() / encodeType;
        for (int i = 0; i < max; i++) {
        	int algorism = hexStringToAlgorism(hexString
                    .substring(i * encodeType, (i + 1) * encodeType));
        	if(algorism>90 || algorism < 48) {
        		result = "";
        		return result;
        	}
            char c = (char) algorism;
            result += c;
        }
        return result;
    } 
	
	
	public static int hexStringToAlgorism(String hex) {
        hex = hex.toUpperCase();
        int max = hex.length();
        int result = 0;
        for (int i = max; i > 0; i--) {
            char c = hex.charAt(i - 1);
            int algorism = 0;
            if (c >= '0' && c <= '9') {
                algorism = c - '0';
            } else {
                algorism = c - 55;
            }
            result += Math.pow(16, max - i) * algorism;
        }
        return result;
    }
	
}
