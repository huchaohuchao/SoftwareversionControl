package com.example.demo.services;

import java.util.List;
import java.util.Map;

import com.example.demo.domain.Dictionary_item;
import com.example.demo.domain.PageBean;
import com.github.pagehelper.PageHelper;

public interface Dictionary_itemService {
	
	/**
	 *	查询数据字典列表
	 * @author hc
	 * @paramMap <String, Object> map
	 * @return PageBean<Dictionary_item>
	 */
	public PageBean<Dictionary_item> queryDictionaryList(Map<String, Object> map);
	
	/**
	 * 删除数据字典
	 * @author hc
	 * @param id
	 */
	public void dictionaryDelete(int id);
	
	/**
	 * 修改数据字典
	 * @author hc
	 * @param dictionary_item
	 */
	public void dictionaryUpdate(Dictionary_item dictionary_item);
	
	/**
	 * 新增数据字典
	 * @author hc
	 * @param dictionary_item
	 */
	public void dictionaryInsert(Dictionary_item dictionary_item);
	
	/**
	 *    查询数据字典
	 * @author HC
	 * @param id
	 * @return Dictionary_item
	 */
	public Dictionary_item queryDictionary(String type);
}
