package com.example.demo.domain;

public class ProjectId_processInstanceId {
    private String processinstanceid;

    private Integer projectid;
    
    public ProjectId_processInstanceId(String processinstanceid , Integer projectid) {
		this.processinstanceid = processinstanceid;
		this.projectid = projectid;
	}

    public String getProcessinstanceid() {
        return processinstanceid;
    }

    public void setProcessinstanceid(String processinstanceid) {
        this.processinstanceid = processinstanceid == null ? null : processinstanceid.trim();
    }

    public Integer getProjectid() {
        return projectid;
    }

    public void setProjectid(Integer projectid) {
        this.projectid = projectid;
    }
}