package com.example.demo.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import com.example.demo.domain.Project_details;

@Mapper
public interface Project_detailsMapper {
	
	//软删除流程实例
	void delectExample (String processInstanceId);
	
	void updateStatus(Project_details record);
	
	Project_details queryProject(String processInstanceId);
	
    int deleteByPrimaryKey(Integer id);

    int insert(Project_details record);
    
    Project_details selectByPrimaryKey(Integer id);

    public int updateByPrimaryKey(Project_details record);
    
    List<Project_details> findAll();
    
    Map<String, Object> queryProjectTypeName(Map <String, Object> condtions); 
}