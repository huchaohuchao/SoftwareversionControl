package com.example.demo.services;

import java.util.List;
import java.util.Map;

import com.example.demo.domain.Sys_permission;

public interface Role_permissionService {
	
	
	/**
	 * 	根据角色ID查询所有此角色拥有的权限ID
	 * @param roleId
	 * @return
	 */
	public List<Integer> getPermissionIdByRoleId(int roleId);
	
	
	/**
	 *       保存角色的权限 
	 * @param map
	 * @return
	 */
	public int savePermissionByRoleId(Map map);

}
