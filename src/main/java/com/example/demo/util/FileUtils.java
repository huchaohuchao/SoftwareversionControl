package com.example.demo.util;

import java.io.File;

public class FileUtils {
	
	 /**
     * 删除单个文件
     *
     * @param filePath
     *            要删除的文件的文件名
     * @return 单个文件删除成功返回true，否则返回false
     */
    public static boolean deleteFile(String filepath) {
    	boolean result = true;
        File file = new File(filepath);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                result =  true;
            } else {
            	result =  false;
            }
        }
        return result;
    }

}
