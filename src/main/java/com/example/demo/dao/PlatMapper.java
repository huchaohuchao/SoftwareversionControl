package com.example.demo.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.example.demo.domain.Plat;

@Mapper
public interface PlatMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Plat record);

    int insertSelective(Plat record);

    Plat selectByPrimaryKey(Integer id);
    
    Plat selectByProcessInstanceId(String processInstanceId);

    int updateByPrimaryKeySelective(Plat record);

    int updateByPrimaryKey(Plat record);
    
    List<Plat> selectPlatByCondtions(Map<String, String> condtions);
    
    List<Map<String, Object>> selectProcessInstanceByPlatId(String platId);
    
    List<Map<String, Object>> selectPlatProcessInstanceByPid(String processInstanceId);
    
    List<Map<String, Object>> selectPlatProcessInstanceByPidType1(String processInstanceId);
    
    List<Map<String, Object>> selectPlatProcessInstanceByProjectPid(String processInstanceId);
}