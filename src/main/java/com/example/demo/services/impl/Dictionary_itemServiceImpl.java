package com.example.demo.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.Dictionary_itemMapper;
import com.example.demo.domain.Dictionary_item;
import com.example.demo.domain.PageBean;
import com.example.demo.services.Dictionary_itemService;
import com.github.pagehelper.PageHelper;

@Service
@Transactional
public class Dictionary_itemServiceImpl implements Dictionary_itemService{
	@Autowired
	private Dictionary_itemMapper dictionary_itemMapper;
	
	/**
	 *	查询数据字典列表
	 * @author hc
	 * @paramMap <String, Object> map
	 * @return PageBean<Dictionary_item>
	 */
	public PageBean<Dictionary_item> queryDictionaryList(Map<String, Object> map){
		PageHelper.startPage((Integer)map.get("pageNum"), (Integer)map.get("pageSize")); 
		List<Dictionary_item> dictionary_item = dictionary_itemMapper.queryDictionaryList(map);
		//获取起始记录条数
		PageBean pb = new PageBean((Integer)map.get("pageNum"), (Integer)map.get("pageSize"), dictionary_itemMapper.queryDictionaryCount(map));
		pb.setList(dictionary_item);
    	return pb;
    }	
	
	/**
	 * 删除数据字典
	 * @author hc
	 * @param id
	 */
	public void dictionaryDelete(int id){
		dictionary_itemMapper.deleteByPrimaryKey(id);
	}
	
	/**
	 * 修改数据字典
	 * @author hc
	 * @param dictionary_item
	 */
	public void dictionaryUpdate(Dictionary_item dictionary_item) {
		dictionary_itemMapper.updateByPrimaryKeySelective(dictionary_item);
	}
	
	/**
	 * 新增数据字典
	 * @author hc
	 * @param dictionary_item
	 */
	public void dictionaryInsert(Dictionary_item dictionary_item) {
		dictionary_itemMapper.insertSelective(dictionary_item);
	}
	
	/**
	 *    查询数据字典
	 * @author HC
	 * @param id
	 * @return Dictionary_item
	 */
	public Dictionary_item queryDictionary(String type) {
		return dictionary_itemMapper.queryDictionary(type);
	}
}
