package com.example.demo.dao;

import org.apache.ibatis.annotations.Mapper;

import com.example.demo.domain.Project_remarks;

@Mapper
public interface Project_remarksMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Project_remarks record);

    int insertSelective(Project_remarks record);

    Project_remarks selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Project_remarks record);

    int updateByPrimaryKey(Project_remarks record);
}