package com.example.demo.controller;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Dictionary_item;
import com.example.demo.domain.PageBean;
import com.example.demo.domain.Sysuser;
import com.example.demo.services.Dictionary_itemService;
import com.example.demo.services.SysroleService;
import com.example.demo.util.Util;

import net.sf.json.JSONObject;

@RestController
@RequestMapping(value = "/dictionary_item")
public class Dictionary_itemController {

	private static Logger logger = Logger.getLogger(SysroleController.class);

	@Autowired
	private Dictionary_itemService dictionary_itemService;

	/**
	 * 查询数据字典列表
	 * 
	 * @author hc
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/dictionaryList")
	public String dictionaryList(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> map = new HashMap();
		JSONObject obj = new JSONObject();
		try {
			map.put("parent_id", request.getParameter("parent_id"));
			map.put("type", request.getParameter("type"));
			map.put("pageNum", Integer.parseInt(request.getParameter("page")));
			map.put("pageSize", Integer.parseInt(request.getParameter("limit")));
			PageBean<Dictionary_item> pb = dictionary_itemService.queryDictionaryList(map);
			if (pb.getList().size() < 0) {
				obj.put(Util.RESULT, Util.RESULT_RC_CHECK_FAIL);
				obj.put(Util.MESSAGE, "查询数据字典列表失败");
			} else {
				obj.put("data", pb.getList());
				obj.put(Util.COUNT, pb.getTotalRecord());
				obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
				obj.put(Util.MESSAGE, "查询数据字典列表成功！");
			}

		} catch (Exception e) {
			logger.error("=============[Dictionary_itemController[dictionaryList[查询数据字典列表失败]]]==========", e);
		}
		return obj.toString();
	}

	/**
	 * 新增或编辑数据字典
	 * 
	 * @author hc
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/dictionaryAdd")
	public String dictionaryAdd(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject obj = new JSONObject();
		try {
			Dictionary_item dictionary_item = new Dictionary_item();
			dictionary_item.setText(request.getParameter("text"));
			dictionary_item.setType(request.getParameter("type"));
			dictionary_item.setValue(request.getParameter("value"));
			if (request.getParameter("parent_id") != null && !"".equals(request.getParameter("parent_id"))) {
				dictionary_item.setParent_id(Integer.parseInt(request.getParameter("parent_id")));
			}
			if (request.getParameter("sort") != null && !"".equals(request.getParameter("sort"))) {
				dictionary_item.setSort(Integer.parseInt(request.getParameter("sort")));
			}
			if (request.getParameter("id") == null || "".equals(request.getParameter("id"))) {
				dictionary_itemService.dictionaryInsert(dictionary_item);
				obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
				obj.put(Util.MESSAGE, "新增数据字典成功！");
			} else {
				dictionary_item.setId(Integer.parseInt(request.getParameter("id")));
				dictionary_itemService.dictionaryUpdate(dictionary_item);
				obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
				obj.put(Util.MESSAGE, "编辑数组字典成功！");
			}
		} catch (Exception e) {
			logger.error("=============[Dictionary_itemController[dictionaryAdd[新增或编辑数据字典失败]]]==========", e);
		}
		return obj.toString();
	}

	/**
	 * 软删除数据字典
	 * 
	 * @author hc
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/dictionaryDelete")
	public String dictionaryDelete(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject obj = new JSONObject();
		try {
			if (request.getParameter("id") != null && !"".equals(request.getParameter("id"))) {
				dictionary_itemService.dictionaryDelete(Integer.parseInt(request.getParameter("id")));
				obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
				obj.put(Util.MESSAGE, "删除数据字典成功！");
			}
		} catch (Exception e) {
			logger.error("=============[Dictionary_itemController[dictionaryDelete[删除数据字典失败]]]==========", e);
		}
		return obj.toString();
	}

	/**
	 *
	 * @author hc
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/dictionaryQuery")
	public String dictionaryQuery(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject obj = new JSONObject();
		try {
			if (request.getParameter("dictionaryType") != null && !"".equals(request.getParameter("dictionaryType"))) {
				Dictionary_item dictionary = dictionary_itemService.queryDictionary(request.getParameter("dictionaryType"));
				obj.put("dictionary", dictionary);
				obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
				obj.put(Util.MESSAGE, "查詢数据字典成功！");
			}
		} catch (Exception e) {
			logger.error("=============[Dictionary_itemController[dictionaryQuery[查詢数据字典失败]]]==========", e);
		}
		return obj.toString();
	}
}
