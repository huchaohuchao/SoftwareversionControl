package com.example.demo.domain;

import java.util.Date;

public class ProcessinstanceFile {
    private Integer id;

	private String filename;

	private String processinstanceid;

	private String url;

	private Date time;

	private String fileremarks;

	private Integer userid;

	private String taskid;
	
	private String status;
	
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename == null ? null : filename.trim();
	}

	public String getProcessinstanceid() {
		return processinstanceid;
	}

	public void setProcessinstanceid(String processinstanceid) {
		this.processinstanceid = processinstanceid == null ? null : processinstanceid.trim();
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url == null ? null : url.trim();
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getFileremarks() {
		return fileremarks;
	}

	public void setFileremarks(String fileremarks) {
		this.fileremarks = fileremarks == null ? null : fileremarks.trim();
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getTaskid() {
		return taskid;
	}

	public void setTaskid(String taskid) {
		this.taskid = taskid == null ? null : taskid.trim();
	}

	@Override
	public String toString() {
		return "ProcessinstanceFile [id=" + id + ", filename=" + filename + ", processinstanceid=" + processinstanceid
				+ ", url=" + url + ", time=" + time + ", fileremarks=" + fileremarks + ", userid=" + userid
				+ ", taskid=" + taskid + ", status=" + status + "]";
	}

}