package com.example.demo.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.example.demo.dao.Project_detailsMapper;
import com.example.demo.domain.Project_details;
import com.example.demo.services.ActivitiService;
import com.example.demo.util.SpringUtils;

@Component
public class MyExecutionListener implements ExecutionListener {
	
	
	
	@Override
    public void notify(DelegateExecution delegateExecution) throws Exception {
		   String eventName = delegateExecution.getEventName();
	        String process_id = delegateExecution.getProcessInstanceId();
	        Project_detailsMapper project_detailsMapper = (Project_detailsMapper) SpringUtils.getObject("project_detailsMapper");
	        if ("start".equals(eventName)) {
	            System.out.println("start=========");
	        }else if ("end".equals(eventName)) {
	            System.out.println("end=========" + process_id);
	            Project_details project_details = new Project_details();
	            project_details.setProcessinstanceid(process_id);
	            project_details.setStatus(2);
	            project_detailsMapper.updateStatus(project_details);
	        } else if ("take".equals(eventName)) {//连线监听
	            System.out.println("take=========");				
	        }
    }
}
