package com.example.demo.domain;

import java.util.Date;

public class Department {
    private Integer dept_id;

    private String dept_name;

    private Date established;

    private Date create_date;

    private String create_by;

    private Date update_date;

    private String update_by;

    public Integer getdept_id() {
        return dept_id;
    }

    public void setDept_id(Integer dept_id) {
        this.dept_id = dept_id;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name == null ? null : dept_name.trim();
    }

    public Date getEstablished() {
        return established;
    }

    public void setEstablished(Date established) {
        this.established = established;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public String getCreate_by() {
        return create_by;
    }

    public void setCreate_by(String create_by) {
        this.create_by = create_by == null ? null : create_by.trim();
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public String getUpdate_by() {
        return update_by;
    }

	public void setUpdate_by(String update_by) {
        this.update_by = update_by == null ? null : update_by.trim();
    }
	
    @Override
	public String toString() {
		return "department [dept_id=" + dept_id + ", dept_name=" + dept_name + ", established=" + established
				+ ", create_date=" + create_date + ", create_by=" + create_by + ", update_date=" + update_date
				+ ", update_by=" + update_by + "]";
	}

}