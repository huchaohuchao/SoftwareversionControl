package com.example.demo.services;

import java.util.List;
import java.util.Map;

import com.example.demo.domain.Plat;
import com.example.demo.domain.ProcessinstanceFile;
import com.example.demo.domain.Project_details;

public interface PlatService {
	
	/**按条件获取平台列表
	 * 
	 * @author dsh
	 * @param conditions 筛选条件
	 * @return List<Plat> 
	 * **/   
	public List<Plat> findPlatByConditions(Map<String,String> condtions);
	
	
	/**按条件获取平台列表
	 * 
	 * @author dsh
	 * @param platName 平台名称
	 * @param platNumber 平台编号
	 * @return void
	 * **/   
	public void addNewPlat(String platName, String platNumber);
	
	
	/**按平台ID获取流程实例，每个流程实例代表一个版本
	 * @author dsh
	 * @param platId 平台名称
	 * @return void
	 * **/  
	public List<Map<String, Object>> selectProcessInstanceByPlatId(String platId);
	
	/**获取平台程序文件
	 * @author dsh
	 * @param platId 平台名称
	 * @return void
	 * **/  
	public List<ProcessinstanceFile> findPlatProcessInstanceFileTableByPid(String processInstanceId);
	
	/**删除平台
	 * @author dsh
	 * @param String platId 平台名称
	 * @return int
	 * **/  
	public int deletePlatById(String platId);
	
	/**根据项目的流程实例ID查找项目对应的平台的所有程序版本
	 * @author dsh
	 * @param String platId 平台名称
	 * @return int
	 * **/  
	public List<Map<String, Object>> selectPlatProcessInstanceByPid(String processInstanceId);
	

}
