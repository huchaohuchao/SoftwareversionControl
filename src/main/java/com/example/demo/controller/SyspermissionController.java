package com.example.demo.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Sys_permission;
import com.example.demo.domain.Sys_permissionList;
import com.example.demo.services.SyspermissionService;
import com.example.demo.util.Util;

import net.sf.json.JSONArray;

@RestController
@RequestMapping("/syspermission")
public class SyspermissionController {
	
	@Autowired
	private SyspermissionService syspermissionService;
	
	
	@RequestMapping("/getSyspermissionTable")
	@ResponseBody
	public Object getSyspermissionTable(HttpServletRequest request) {
		Map<String,Object> obj = new HashMap<String,Object>();
		Map<String,String> conditions = new HashMap();//获取前台传来的参数
		String patrent_id = request.getParameter("parent_id") == null? "" : request.getParameter("parent_id");
		String id = request.getParameter("id") == null? "" : request.getParameter("id");
		conditions.put("parent_id", patrent_id);
		conditions.put("id", id);
		List<Sys_permission> data = syspermissionService.querySyspermissionTable(conditions);//通过条件获取权限列表
		obj.put("data", data);
		obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
		obj.put(Util.MESSAGE, "查询权限列表成功！");
		return obj;
	}
	
	
	
	@RequestMapping("/getSyspermissionSelect")
	@ResponseBody
	public Object getSyspermissionSelect(HttpServletRequest request) {
		Map<String,Object> obj = new HashMap<String,Object>();
		List<Sys_permission> data = syspermissionService.querySyspermissionSelect();//获取所有1.2级权限对象
		obj.put("data", data);
		return obj;
	}
	
	
	@RequestMapping("/getSyspermissionByMenuId")
	@ResponseBody
	public Object getSyspermissionSelect(String menuId) {
		Map<String,Object> obj = new HashMap<String,Object>();
		Map<String, String> conditions = new HashMap<String,String>();
		conditions.put("menuId", menuId);
		List<Sys_permission> data = syspermissionService.querySyspermissionByMenuId(conditions);//查询所有的父id为menuId的权限对象
		obj.put("data", data);
		return obj;
	}
	
	@RequestMapping("/saveSyspermission")
	public int saveSyspermission(@RequestBody Sys_permission sys_permission) {
		int result = 0;
		try {
			result = syspermissionService.saveSyspermission(sys_permission);
		}catch (Exception e) {
			e.toString();
		}	
		return result;
	}
	
	
	@RequestMapping("/updateSyspermission")
	public int updateSyspermission(@RequestBody Sys_permission sys_permission) {
		int result = 0;
		try {
			result = syspermissionService.updateSyspermission(sys_permission);
		}catch (Exception e) {
			e.toString();
		}	
		return result;
	}
	
	@RequestMapping("/deleteSyspermissionById")
	public int deleteSyspermissionById(int id) {
		int result = 0;
		try {
			result = syspermissionService.deleteSyspermissionById(id);
		}catch (Exception e) {
			e.toString();
		}	
		return result;
	}

}
