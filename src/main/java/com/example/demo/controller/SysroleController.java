package com.example.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Department;
import com.example.demo.domain.PageBean;
import com.example.demo.domain.Position;
import com.example.demo.domain.Project_type;
import com.example.demo.domain.Sysrole;
import com.example.demo.domain.Sysuser;
import com.example.demo.services.SysroleService;
import com.example.demo.services.SysuserService;
import com.example.demo.util.Util;

import net.sf.json.JSONObject;

@RestController
@RequestMapping(value = "/sysrole")
public class SysroleController {

private static Logger logger = Logger.getLogger(SysroleController.class);
	
	@Autowired
	private SysroleService sysroleService;
	
   /**
    *	获取角色下拉框
    * @author hc
	* @param request
	* @param response
	* @throws Exception
    */
    @RequestMapping(value = "/roleSelection")
    public String roleSelection(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	JSONObject obj=new JSONObject();
    	try{
    		List<Sysrole> sysrole = sysroleService.roleSelection();
    		obj.put("sysrole", sysrole);
			obj.put(Util.RESULT, Util.RESULT_RC_CHECK_FAIL);
			obj.put(Util.MESSAGE, "查询角色列表失败");
		} catch (Exception e) {
			logger.error("=============[SysuserController[userList[获取角色下拉框失败]]]==========",e);
		}
    	return obj.toString();
    }
    
    /**
     *	获取部门下拉框
     * @author hc
 	* @param request
 	* @param response
 	* @throws Exception
     */
     @RequestMapping(value = "/departmentSelection")
     public String departmentSelection(HttpServletRequest request, HttpServletResponse response) throws Exception{
     	JSONObject obj=new JSONObject();
     	try{
     		List<Department> department = sysroleService.departmentSelection();
     		obj.put("department", department);
 			obj.put(Util.RESULT, Util.RESULT_RC_CHECK_FAIL);
 			obj.put(Util.MESSAGE, "查询部门列表成功");
 		} catch (Exception e) {
 			logger.error("=============[SysuserController[userList[获取部门下拉框失败]]]==========",e);
 		}
     	return obj.toString();
     }
     
     /**
      *	获取部门员工下拉框
      * @author hc
  	* @param request
  	* @param response
  	* @throws Exception
      */
      @RequestMapping(value = "/departmentStaffSelection")
      @ResponseBody
      public Object departmentStaffSelection(HttpServletRequest request, HttpServletResponse response) throws Exception{
      	 Map<String, Object> obj = new  HashMap<String, Object>();
      	try{
      		String department_id = request.getParameter("departmen_id");
      		String isNextApprovel = request.getParameter("isNextApprovel") == null ? "1" : request.getParameter("isNextApprovel");
      		System.out.println(isNextApprovel);
      		List<Sysuser> sysuser = sysroleService.departmentStaffSelection(department_id,isNextApprovel);
      		obj.put("sysuser", sysuser);
  			obj.put(Util.RESULT, Util.RESULT_RC_CHECK_FAIL);
  			obj.put(Util.MESSAGE, "查询人员列表成功");
  		} catch (Exception e) {
  			logger.error("=============[SysuserController[userList[获取部门下拉框失败]]]==========",e);
  		}
      	return obj;
      }
      
	/**
	 * 获取项目类型下拉框
	 * 
	 * @author hc
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/projectTypeSelection")
	public String projectTypeSelection(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject obj = new JSONObject();
		try {
			List<Project_type> project_type = sysroleService.projectTypeSelection();
			obj.put("project_type", project_type);
			obj.put(Util.RESULT, Util.RESULT_RC_CHECK_FAIL);
			obj.put(Util.MESSAGE, "获取项目类型下拉框成功");
		} catch (Exception e) {
			logger.error("=============[SysuserController[projectTypeSelection[获取项目类型下拉框失败]]]==========", e);
		}
		return obj.toString();
	}
	
	/**
	 * 获取职位类型下拉框
	 * 
	 * @author DSH
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/positionSelection")
	public String positionSelection(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject obj = new JSONObject();
		try {
			List<Position> position = sysroleService.positionSelection();
			obj.put("position", position);
			obj.put(Util.RESULT, Util.RESULT_RC_CHECK_FAIL);
			obj.put(Util.MESSAGE, "获取项目类型下拉框成功");
		} catch (Exception e) {
			logger.error("=============[SysuserController[projectTypeSelection[获取项目类型下拉框失败]]]==========", e);
		}
		return obj.toString();
	}
	
	
	/**
	 * 获取职位类型下拉框
	 * 
	 * @author hc
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/projectTypeSelectionByDepartmentId")
	@ResponseBody
	public Object projectTypeSelectionByDepartmentId(HttpServletRequest request, HttpServletResponse response, int departmentId) throws Exception {
		List<Map<String, Object>> project_type = null;
		try {
			project_type = sysroleService.projectTypeSelectionByDepartmentId(departmentId);
		} catch (Exception e) {
			logger.error("=============[SysuserController[projectTypeSelection[获取项目类型下拉框失败]]]==========", e);
		}
		return project_type;
	}
}
