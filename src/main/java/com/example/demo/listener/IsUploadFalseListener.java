package com.example.demo.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

public class IsUploadFalseListener implements ExecutionListener{
	
	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// TODO Auto-generated method stub
		execution.setVariable("isUpload", false);//true代表是下一节点是部门,false代表不是
	}

}
