package com.example.demo.services;

import java.util.List; 
import java.util.Map;

import org.activiti.engine.task.Attachment;

import com.example.demo.domain.PageBean;
import com.example.demo.domain.ProcessinstanceFile;
import com.example.demo.domain.ProcessinstanceFileHistory;
import com.example.demo.domain.Project_details;

public interface ActivitiService {
	/**
	 * 	查询所有进行中的任务
	 * @author hc
	 * @param Map <String, Object> map
	 * @return PageBean<Map <String, Object>> 
	 */
	public PageBean<Map <String, Object>> taskListAll(Map<String, Object> map);
	
	/**
	 * 	查询用户进行中的任务
	 * @author hc
	 * @param Map <String, Object> map
	 * @return PageBean<Map <String, Object>> 
	 */
	public PageBean<Map <String, Object>> taskList(Map<String, Object> map);
	
	/**
	 * 	完成任务
	 * @author hc
	 * @param String task_id, String number, int user_id
	 * @return
	 */
	public Map <String, Object> finishTask(Map <String, Object> map);
	
	/**
	 * 	新建项目
	 * @author hc
	 * @param Map <String, Object> map
	 * @return Map <String, Object> map
	 */
	public Map <String, Object> newProject(Map <String, Object> map);
	
	/**
	 * 	查询流程插座记录
	 * @author hc
	 * @param List<Map<String, Object>>
	 * @return String project_id
	 */
	public List<Map<String, Object>> operationRecord(String project_id);
	
	/**
	 * 	删除项目
	 * @author hc
	 * @param Map <String, Object> map
	 * @return 
	 */
	public void delectExample(Map <String, Object> map);
	
	/**
	 * 	添加流程变量
	 * @author dsh
	 * @param taskId ,variables,processInstanceId
	 * @return 
	 */
	public void addVariables(Map <String, Object> variables, String taskId, String processInstanceId);
	
	/**
	 * 	添加附件
	 * @author dsh
	 * @param taskId ,attachments,processInstanceId
	 * @return 
	 */
	public void addAttachments(Map <String, Object> attachments, String taskId, String processInstanceId);
	
	/**
	 * 	完成任务two
	 * @author dsh
	 * @param isAllowed ,taskId, assignee
	 * @return 
	 */
	public void finishTaskTwo(Map<String, Object> map);
	
	/**
	 * 	查找流程实例的历史节点信息
	 * @author dsh
	 * @param processInstanceId
	 * @return List<Map<String, Object>>
	 */
	public List<Map<String, Object>> findProcessInstanceHistoryRecord(String processInstanceId );
	
	/**
	 * 	查找taskId的所有附件
	 * @author dsh
	 * @param taskId
	 * @return List<Attachment>
	 */
	public List<Attachment> findTaskAttachmentsTable(String taskId);
	
	/**
	 * 	根据Id删除附件
	 * @author dsh
	 * @param attachmentId
	 * @return void
	 */
	public void deleteAttachmentById(String attachmentId);

	/**
	 * 	查询历史流程记录
	 * @author hc
	 * @param PageBean<Map <String, Object>>
	 * @return List<Map<String, Object>>
	 */
	public List<Map<String, Object>> historyProjectList(Map <String, Object> map);

	/**
	 * 	更改项目状态
	 * @author hc
	 * @param Project_details project_details
	 * @return void
	 */
	public void updateStatus(Project_details project_details);
	
	/**
	 * 	查询此流程实例关联的所有全局文件
	 * @author dsh
	 * @param processInstanceId 流程实例ID
	 * @return List<ProcessinstanceFile>
	 */
	public List<ProcessinstanceFile> findGlobalFIleByPid(String processInstanceId);
	
	/**
	 * 	添加全局文件
	 * @author dsh
	 * @param processInstanceId 流程实例ID, taskId 任务ID
	 * @return void
	 */
	public void addGlobalFIle(String processInstanceId,String taskId,String url, String fileName, String processInstanceFileId);
	
	/**
	 * 	查询全局文件的所有历史版本
	 * @author dsh
	 * @param processInstanceFileId 全局文件对应的id
	 * @return void
	 */
	public List<Map<String, Object>> findSingleGlobalFIleHisById(String processInstanceFileId);
	
	/**
	 * 	查询流程实例所有先关的附件
	 * @author dsh
	 * @param processInstanceId 流程实例id
	 * @return List<Attachment>
	 */
	public List<Attachment> findAttachmentsTableByPid(String processInstanceId);
	
	/**
	 * 	判断此节点是否有权限驳回
	 * @author dsh
	 * @param taskId 任务节点ID
	 * @return String
	 */
	public boolean findRejectStatusByTaskId(String taskId);
	
	/**
	 * 	判断下一节点是否是针对部门
	 * @author dsh
	 * @param taskId 任务节点ID
	 * @return boolean
	 */
	public boolean findIsDepartment(String taskId);
	
	/**
	 * 	查看此节点是否有上传程序和标定数据文件的权限
	 * @author dsh
	 * @param taskId 任务节点ID
	 * @return boolean
	 */
	public boolean findIsUpload(String taskId);
	
	/**
	 * 	根据任务ID查找下一任务的名称
	 * @author dsh
	 * @param taskId 任务节点ID
	 * @return String
	 */
	 public Map<String,Object> getNextTaskName(String taskId);
	 
	 /**
		 * 	根据任务ID查找下一任务的名称
		 * @author dsh
		 * @param 任务节点ID
		 * @return Map<String,Object>
		 */
	 public Map<String,Object> findProcessInstanceVariables(String taskId);
	 
	 /**
		 * 	改变已读未读状态
		 * @author dsh
		 * @param 任务节点ID
		 * @return Map<String,Object>
		 */
	 public void changeIsProcessInstanceRead(String taskId);
	 
	 /**
		 * 	删除全局文件
		 * @author dsh
		 * @param id 文件id url 文件路径
		 * @return int
		 */
	 public int deleteGlobalFile(int id, String url);
	 
}
