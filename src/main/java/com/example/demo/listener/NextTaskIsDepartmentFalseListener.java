package com.example.demo.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

public class NextTaskIsDepartmentFalseListener implements ExecutionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		execution.setVariable("isDepartment", false);//true代表是下一节点是部门,false代表不是
	}

}
