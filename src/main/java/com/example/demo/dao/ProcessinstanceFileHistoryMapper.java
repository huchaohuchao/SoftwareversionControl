package com.example.demo.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.example.demo.domain.ProcessinstanceFileHistory;

@Mapper
public interface ProcessinstanceFileHistoryMapper {
    int deleteByPrimaryKey(Integer id);

	int insert(ProcessinstanceFileHistory record);

	int insertSelective(ProcessinstanceFileHistory record);

	ProcessinstanceFileHistory selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(ProcessinstanceFileHistory record);

	int updateByPrimaryKey(ProcessinstanceFileHistory record);
	
	List<Map<String, Object>> selectByProcessInstanceFileId(String processInstanceFileId);

}