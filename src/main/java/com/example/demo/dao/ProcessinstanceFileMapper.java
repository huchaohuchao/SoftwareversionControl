package com.example.demo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.example.demo.domain.ProcessinstanceFile;

@Mapper
public interface ProcessinstanceFileMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProcessinstanceFile record);

    int insertSelective(ProcessinstanceFile record);

    ProcessinstanceFile selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProcessinstanceFile record);

    int updateByPrimaryKey(ProcessinstanceFile record);
    
    List<ProcessinstanceFile> selectProcessinstanceFileByPid(String processInstanceId);
    
    ProcessinstanceFile selectProcessinstanceFileByUrl(String url);
    
    List<ProcessinstanceFile> selectProjectProcessInstanceFileTable(@Param("projectId")String projectId, @Param("type")String type);
    
    List<ProcessinstanceFile> selectProcessinstanceListFileByPid(String processInstanceId);
    
    int selectTypeByProcessDefinitionId(String processDefinitionId);
}