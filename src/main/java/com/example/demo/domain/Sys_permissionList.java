package com.example.demo.domain;

import java.util.List;

public class Sys_permissionList {
	
	private List<Sys_permission> sys_permissionList;

	public List<Sys_permission> getSys_permissionList() {
		return sys_permissionList;
	}

	public void setSys_permissionList(List<Sys_permission> sys_permissionList) {
		this.sys_permissionList = sys_permissionList;
	}

}
