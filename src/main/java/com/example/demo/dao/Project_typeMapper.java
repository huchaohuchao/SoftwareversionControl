package com.example.demo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.example.demo.domain.Project_type;

@Mapper
public interface Project_typeMapper {
	
	//获取项目类型下拉框
	List<Project_type> projectTypeSelection();
	
    int deleteByPrimaryKey(Integer id);

    int insert(Project_type record);

    int insertSelective(Project_type record);

    Project_type selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Project_type record);

    int updateByPrimaryKey(Project_type record);
}