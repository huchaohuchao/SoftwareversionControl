package com.example.demo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.example.demo.domain.Sysrole;

@Mapper
public interface SysroleMapper {

	//角色下拉框
	List<Sysrole> roleSelection();
	
	int deleteByPrimaryKey(Integer id);


	int insertSelective(Sysrole record);


	int updateByPrimaryKeySelective(Sysrole record);

}