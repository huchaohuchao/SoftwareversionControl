package com.example.demo.domain;

public class Project_type {
    private Integer id;

    private String type;
    
    private String project_key;


	public String getProject_key() {
		return project_key;
	}

	public void setProject_key(String project_key) {
		this.project_key = project_key;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

	public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }
	

    @Override
	public String toString() {
		return "Project_type [id=" + id + ", type=" + type + ", project_key=" + project_key + "]";
	}

}