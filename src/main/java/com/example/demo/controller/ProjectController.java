package com.example.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.activiti.engine.task.Attachment;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.PlatMapper;
import com.example.demo.dao.ProjectId_processInstanceIdMapper;
import com.example.demo.dao.ProjectMapper;
import com.example.demo.dao.Project_detailsMapper;
import com.example.demo.domain.Plat;
import com.example.demo.domain.ProcessinstanceFile;
import com.example.demo.domain.ProcessinstanceFileHistory;
import com.example.demo.domain.Project;
import com.example.demo.domain.Project_details;
import com.example.demo.services.ActivitiService;
import com.example.demo.services.ProjectService;
import com.example.demo.util.Util;
import com.github.pagehelper.PageInfo;

@RestController
@RequestMapping("/project")
public class ProjectController {
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private ActivitiService activitiService;
	
	@Autowired
	private PlatMapper platMapper;
	
	private static Logger logger = Logger.getLogger(ProjectController.class);
	
	/**
	 * by dsh
	 * 获取所有项目
	 * param 
	 * return List<Project>
	 * **/
	@RequestMapping("/getProjectSelection")
	@ResponseBody
	public Object getProjectSelection(String condtions) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		try {
			if(condtions.equals("1")) {
				List<Plat> paltList = platMapper.selectPlatByCondtions(new HashMap<String,String>());
			    obj.put("data", paltList);
			}else {
				List<Project> projectList = projectService.getAllProject();
			    obj.put("data", projectList);
			}
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "查询成功！");
			
		}catch (Exception e) {
			logger.error(e);
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "查询失败！");
			logger.error("=============[ActivitiController[getProjectSelection[查询失败]]]==========", e);
		}				
		return obj;
	}
	
	
	/**
	 * by dsh
	 * 按条件获取项目
	 * param 
	 * return List<Project>
	 * **/
	@RequestMapping("/findProjectByConditions")
	@ResponseBody
	public Object findProjectByConditions(HttpServletRequest request) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		Map<String,Object> condtions = new HashMap<String,Object>();//条件
		String projectName = request.getParameter("projectName") == null ? "" : request.getParameter("projectName");
		String projectNumber = request.getParameter("projectNumber") == null ? "" : request.getParameter("projectNumber");
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int pageSize = Integer.parseInt(request.getParameter("limit"));
		condtions.put("projectName", projectName);
		condtions.put("projectNumber", projectNumber);
		condtions.put("pageNum", pageNum);
		condtions.put("pageSize", pageSize);
		PageInfo<Project> projectList = null;
		try {
			projectList = projectService.findProjectByConditions(condtions);
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "查询项目失败！");
			obj.put("data", projectList.getList());
			obj.put("count", projectList.getTotal());
		}catch (Exception e) {
			logger.error(e);
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "查询项目失败！");
			logger.error("=============[ProjectController[findProjectByConditions[查询全局文件失败]]]==========", e);
		}				
		return obj;
	}
	
	/**
	 * by dsh
	 * 按条件获取项目
	 * param 
	 * return List<Project>
	 * **/
	@RequestMapping("/addProject")
	@ResponseBody
	public Object addProject(HttpServletRequest request,String projectName, String projectNumber, String platId) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		try {
			projectService.addProject(projectName, projectNumber, platId);;
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "查询项目成功！");
		}catch (Exception e) {
			logger.error(e);
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "查询项目失败！");
			logger.error("=============[ProjectController[addProject[查询全局文件失败]]]==========", e);
		}				
		return obj;
	}
		
	/**
	 * by dsh
	 * 按条件获取项目
	 * param 
	 * return List<Project>
	 * **/
	@RequestMapping("/deleteProject")
	@ResponseBody
	public Object deleteProject(HttpServletRequest request,String projectId) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		try {
			int result = projectService.deleteProject(projectId);
			if(result > 0) {
				obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
				obj.put(Util.MESSAGE, "删除项目成功！");
			}	else {
				obj.put(Util.RESULT, Util.RESULT_RC_NO_DATA);
				obj.put(Util.MESSAGE, "删除项目失败！");
			}
		}catch (Exception e) {
			logger.error(e);
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "查询项目失败！");
			logger.error("=============[ProjectController[deleteProject[删除项目失败]]]==========", e);
		}				
		return obj;
	}
	
	/**
	 * by dsh
	 * 获取一个项目的所有文件
	 * param 
	 * return List<Project>
	 * **/
	@RequestMapping("/findprojectProcessInstanceFileTable")
	@ResponseBody
	public Object findprojectProcessInstanceFileTable(HttpServletRequest request) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		String projectId = request.getParameter("projectId") == null ? "" : request.getParameter("projectId");
		String type = request.getParameter("type") == null ? "" : request.getParameter("type");
		//String processInstanceId = request.getParameter("processInstanceId") == null ? "" : request.getParameter("processInstanceId");
		List<ProcessinstanceFile> processinstanceFileList = null;
		try {
			if(!projectId.equals("")) {
				processinstanceFileList = projectService.findprojectProcessInstanceFileTable(projectId, type);			
			}			
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "查询文件成功！");
			obj.put("data", processinstanceFileList);
		}catch (Exception e) {
			logger.error(e);
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "查询文件失败！");
			logger.error("=============[ProjectController[findprojectProcessInstanceFileTable[查询文件失败]]]==========", e);
		}				
		return obj;
	}
		
	
	/**
	 * by dsh
	 * 获取文件的所有历史版本
	 * param 
	 * return List<Project>
	 * **/
	@RequestMapping("/findProcessInstanceFileHistTable")
	@ResponseBody
	public Object findProcessInstanceFileHistTable(HttpServletRequest request) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		String processInstanceFileId = request.getParameter("processInstanceFileId") == null ? "" : request.getParameter("processInstanceFileId");
		List<Map<String, Object>> processinstanceFileHistoryList = null;
		try {
			if(!processInstanceFileId.equals("")) {
				processinstanceFileHistoryList = projectService.findProcessInstanceFileHistTable(processInstanceFileId);			
			}			
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "查询历史版本成功！");
			obj.put("data", processinstanceFileHistoryList);
		}catch (Exception e) {
			logger.error(e);
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "查询历史版本失败！");
			logger.error("=============[ProjectController[findProcessInstanceFileHistTable[查询历史版本失败]]]==========", e);
		}				
		return obj;
	}
	
	/**找到该流程所有附件
	 * @author dell dsh<br>
	 * @param projectId 项目ID <br>
	 * @param status 文件状态<br>
	 * @return List<ProcessinstanceFileHistory> 
	 * **/
	@RequestMapping("/findattachmentsTableByPid")
	@ResponseBody
	public Object findattachmentsTableByPid(HttpServletRequest request) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		String processInstanceId = request.getParameter("processInstanceId") == null ? "" : request.getParameter("processInstanceId");
		List<Attachment> attachmentList = null;
		try {
			if(!processInstanceId.equals("")) {
				attachmentList = activitiService.findAttachmentsTableByPid(processInstanceId);		
			}			
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "查询历史版本成功！");
			obj.put("data", attachmentList);
		}catch (Exception e) {
			logger.error(e);
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "查询历史版本失败！");
			logger.error("=============[ProjectController[findProcessInstanceFileHistTable[查询历史版本失败]]]==========", e);
		}				
		return obj;
	}
	
	/**判断项目名字是否存在
	 * **/
	@RequestMapping("/isProjectNameExist")
	@ResponseBody
	public Object isProjectNameExist(HttpServletRequest request,String projectName,String projectNumber) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		String data = "0 ";	
		try {		
			List<Project> projectList = projectService.getAllProject();
			if(projectList.size() > 0) {
				for(Project project: projectList) {
					if(project.getProjectName().equals(projectName) || project.getProjectNumber().equals(projectNumber)) {
						data = "1";
						break;
					}
				}
			}			
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "查询历史版本成功！");
			obj.put("data", data);
		}catch (Exception e) {
			logger.error(e);
			obj.put("data", "2");
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "查询历史版本失败！");
			logger.error("=============[ProjectController[findProcessInstanceFileHistTable[查询历史版本失败]]]==========", e);
		}				
		return obj;
	}
	
	
	/**查询某个项目的所有关联流程
	 * **/
	@RequestMapping("/findProcessInstanceByprojectId")
	@ResponseBody
	public Object findProcessInstanceByprojectId(HttpServletRequest request,String projectName,String projectNumber) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		String projectId = request.getParameter("projectId") == null ? "" : request.getParameter("projectId");
		String type = request.getParameter("type") == null ? "" : request.getParameter("type");
		try {		
			List<Map<String, Object>> result = projectService.findProcessInstanceByprojectId(projectId, type);		
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "查询某个项目的所有关联流程！");
			obj.put("data", result);
		}catch (Exception e) {
			logger.error(e);
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "查询历史版本失败！");
			logger.error("=============[ProjectController[findProcessInstanceFileHistTable[查询某个项目的所有关联流程]]]==========", e);
		}				
		return obj;
	}
	
	/**找到此流程所属的项目下的所有流程
	 * @author dell dsh<br>
	 * @param projectId 项目ID <br>
	 * @param status 文件状态<br>
	 * @return List<ProcessinstanceFileHistory> 
	 * **/
	@RequestMapping("/findProjecProcessInstanceByProjectPid")
	@ResponseBody
	public Object findProjecProcessInstanceByProjectPid(HttpServletRequest request) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		String processInstanceId = request.getParameter("processInstanceId") == null ? "" : request.getParameter("processInstanceId");
		try {			
			int projectId = projectService.findProjectIdPid(processInstanceId);
			List<Map<String, Object>> result = projectService.findProcessInstanceByprojectId(projectId+"", "2");//2代表标定数据
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "查询历史版本成功！");
			obj.put("data", result);
		}catch (Exception e) {
			logger.error(e);
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "查询历史版本失败！");
			logger.error("=============[ProjectController[findProcessInstanceFileHistTable[查询历史版本失败]]]==========", e);
		}				
		return obj;
	}

}
