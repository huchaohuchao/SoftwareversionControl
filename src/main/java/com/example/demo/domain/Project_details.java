package com.example.demo.domain;

public class Project_details {
    private Integer id;

    private String name;

    private String processinstanceid;
    
    private Integer status;
    
    private Integer type;
    
    private String version;

    
    public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getProcessinstanceid() {
        return processinstanceid;
    }

    public void setProcessinstanceid(String processinstanceid) {
        this.processinstanceid = processinstanceid == null ? null : processinstanceid.trim();
    }

	@Override
	public String toString() {
		return "Project_details [id=" + id + ", name=" + name + ", processinstanceid=" + processinstanceid + ", status="
				+ status + ", type=" + type + ", version=" + version + "]";
	}	
}