package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.PlatMapper;
import com.example.demo.domain.Plat;
import com.example.demo.domain.ProcessinstanceFile;
import com.example.demo.domain.Project;
import com.example.demo.services.PlatService;
import com.example.demo.services.ProjectService;
import com.example.demo.util.Util;

@RestController
@RequestMapping("/plat")
public class PlatController {
	
	@Autowired
	private PlatService platService;
	@Autowired
	private PlatMapper platMapper;
	
	private static Logger logger = Logger.getLogger(PlatController.class);
	
	/**按条件获取平台列表
	 * 
	 * @author dsh
	 * @param conditions 筛选条件
	 * @return List<Plat> 
	 * **/   
	@RequestMapping("/findPlatByConditions")
	@ResponseBody
	public Object findPlatByConditions(HttpServletRequest request) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		Map<String,String> condtions = new HashMap<String,String>();//条件
		String platName = request.getParameter("platName") == null ? "" : request.getParameter("platName");
		String platNumber = request.getParameter("platNumber") == null ? "" : request.getParameter("platNumber");
		condtions.put("platName", platName);
		condtions.put("platNumber", platNumber);
		List<Plat> platList = null;
		try {
			platList = platService.findPlatByConditions(condtions);
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "查询平台成功！");
			obj.put("data", platList);
		}catch (Exception e) {
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "查询平台失败！");
			logger.error("=============[PlatController[findPlatByConditions[查询平台失败]]]==========", e);
		}				
		return obj;
	}
	
	/**按条件获取平台列表
	 * 
	 * @author dsh
	 * @param conditions 筛选条件
	 * @return List<Plat> 
	 * **/   
	@RequestMapping("/addNewPlat")
	@ResponseBody
	public Object addNewPlat(HttpServletRequest request, String platName, String platNumber) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		List<Plat> platList = null;
		try {
			platService.addNewPlat(platName, platNumber);
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "添加平台成功！");
		}catch (Exception e) {
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "添加平台失败！");
			logger.error("=============[PlatController[findPlatByConditions[查询平台失败]]]==========", e);
		}				
		return obj;
	}
	
	/**按条件获取平台列表
	 * 
	 * @author dsh
	 * @param conditions 筛选条件
	 * @return List<Plat> 
	 * **/   
	@RequestMapping("/selectProcessInstanceByPlatId")
	@ResponseBody
	public Object selectProcessInstanceByPlatId(HttpServletRequest request) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		String platId = request.getParameter("platId") == null ? "" : request.getParameter("platId");
		try {
			if(!platId.equals("")) {
				List<Map<String, Object>> list = platService.selectProcessInstanceByPlatId(platId);
				obj.put("data", list);
			}
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "查询成功！");			
		}catch (Exception e) {
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "查询失败！");
			logger.error("=============[PlatController[findPlatByConditions[查询失败]]]==========", e);
		}				
		return obj;
	}
	
	
	/**按流程实例ID获取该流程实例的文件
	 * 
	 * @author dsh
	 * @param conditions 筛选条件
	 * @return List<Plat> 
	 * **/   
	@RequestMapping("/findPlatProcessInstanceFileTableByPid")
	@ResponseBody
	public Object findPlatProcessInstanceFileTableByPid(HttpServletRequest request) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		String processInstanceId = request.getParameter("processInstanceId") == null ? "" : request.getParameter("processInstanceId");
		try {
			if(!processInstanceId.equals("")) {
				List<ProcessinstanceFile> list = platService.findPlatProcessInstanceFileTableByPid(processInstanceId);
				obj.put("data", list);
			}
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "添加平台成功！");			
		}catch (Exception e) {
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "查询失败！");
			logger.error("=============[PlatController[findPlatByConditions[查询失败]]]==========", e);
		}				
		return obj;
	}
	
	/**按流程实例Id查询平台文件
	 * 
	 * @author dsh
	 * @param conditions 筛选条件
	 * @return List<Plat> 
	 * **/   
	@RequestMapping("/selectPlatProcessInstanceByPlatId")
	@ResponseBody
	public Object selectProcessInstanceByPlatId(HttpServletRequest request, String processInstanceId, String type) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		try {
			List<Map<String, Object>> list  = null;
			if(!type.equals("1")) {
				list = platMapper.selectPlatProcessInstanceByPid(processInstanceId);
			}else {
				list = platMapper.selectPlatProcessInstanceByPidType1(processInstanceId);
			}			
			List<Map<String, Object>> list1 = new ArrayList<Map<String, Object>>();
			if(list.size() > 0) {
				list1.add(list.get(0));
			}			
			obj.put("data", list1);
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "添加平台成功！");			
		}catch (Exception e) {
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "查询失败！");
			logger.error("=============[PlatController[findPlatByConditions[查询失败]]]==========", e);
		}				
		return obj;
	}
	
	/*判断平台名称或者编号是否存在**/   
	@RequestMapping("/isPlatNameExist")
	@ResponseBody
	public Object isPlatNameExist(HttpServletRequest request, String platName, String platNumber) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		String data = "0 ";	
		try {		
			List<Plat> platList = platService.findPlatByConditions(new HashMap<String,String>());
			if(platList.size() > 0) {
				for(Plat plat: platList) {
					if(plat.getName().equals(platName) || plat.getNumber().equals(platNumber)) {
						data = "1";
						break;
					}
				}
			}			
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "查询历史版本成功！");
			obj.put("data", data);
		}catch (Exception e) {
			logger.error(e);
			obj.put("data", "2");
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "查询历史版本失败！");
			logger.error("=============[PlatController[isPlatNameExist[]]]==========", e);
		}				
		return obj;
	}
	
	/*删除平台**/   
	@RequestMapping("/deletePlatById")
	@ResponseBody
	public Object deletePlatById(HttpServletRequest request, String platId) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		try {		
			int result = platService.deletePlatById(platId);		
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "删除平台成功！");
			obj.put("data", result);
		}catch (Exception e) {
			logger.error(e);
			obj.put("data", "0");
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "删除平台失败！");
			logger.error("=============[PlatController[deletePlatById[删除平台失败]]]==========", e);
		}	
		return obj;		
	}
	
	/*根据项目的流程实例ID查找项目对应的平台的所有程序版本**/   
	@RequestMapping("/selectPlatProcessInstanceByProjectPid")
	@ResponseBody
	public Object selectPlatProcessInstanceByProjectPid(HttpServletRequest request, String processInstanceId) {
		Map<String,Object> obj = new HashMap<String,Object>();//传递给后台的对象
		try {		
			List<Map<String, Object>> result = platService.selectPlatProcessInstanceByPid(processInstanceId);	
			obj.put(Util.RESULT, Util.RESULT_RC_SUCCESS);
			obj.put(Util.MESSAGE, "查询平台流程！");
			obj.put("data", result);
		}catch (Exception e) {
			logger.error(e);
			obj.put(Util.RESULT, Util.RESULT_RC_EXCEPTION);
			obj.put(Util.MESSAGE, "删除平台失败！");
			logger.error("=============[PlatController[deletePlatById[查询平台流程]]]==========", e);
		}	
		return obj;		
	}
}
