package com.example.demo.services;

import java.util.List;
import java.util.Map;

import com.example.demo.domain.Department;
import com.example.demo.domain.Position;
import com.example.demo.domain.Project_type;
import com.example.demo.domain.Sysrole;
import com.example.demo.domain.Sysuser;

public interface SysroleService {
	
	/**
	 *	角色下拉框
	 * @author hc
	 * @param 
	 * @return List<Sysrole>
	 */
	List<Sysrole> roleSelection();
	
	/**
	 *	部门下拉框
	 * @author hc
	 * @param 
	 * @return List<Department>
	 */
	List<Department> departmentSelection();
	
	/**
	 *	根据部门id查询部门人员
	 * @author hc
	 * @param String department_id
	 * @return List<Sysuser>
	 */
	public List<Sysuser> departmentStaffSelection(String department_id, String isNextApprovel);
	
	/**
	 *	获取项目类型下拉框
	 * @author hc
	 * @param 
	 * @return List<Project_type>
	 */
	public List<Project_type> projectTypeSelection();
	
	/**
	 *	获取职位类型下拉框
	 * @author dsh
	 * @param 
	 * @return List<Project_type>
	 */
	public List<Position> positionSelection();
	
	
	/**
	 *	根据部门获取项目类型下拉框
	 * @author hc
	 * @param 
	 * @return List<Project_type>
	 */
	public List<Map<String,Object>> projectTypeSelectionByDepartmentId(int departmentId);
}
