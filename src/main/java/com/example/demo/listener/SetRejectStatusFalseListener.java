package com.example.demo.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

public class SetRejectStatusFalseListener implements ExecutionListener{

	
	private static final long serialVersionUID = 1L;

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		execution.setVariable("rejectStatus", "1");//0代表可以驳回，1代表不能驳回		
	}

}
