package com.example.demo.services.impl;

import java.util.List;
import java.util.Map;

import org.activiti.engine.task.Attachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ProcessinstanceFileHistoryMapper;
import com.example.demo.dao.ProcessinstanceFileMapper;
import com.example.demo.dao.ProjectId_processInstanceIdMapper;
import com.example.demo.dao.ProjectMapper;
import com.example.demo.domain.PageBean;
import com.example.demo.domain.ProcessinstanceFile;
import com.example.demo.domain.ProcessinstanceFileHistory;
import com.example.demo.domain.Project;
import com.example.demo.domain.ProjectId_processInstanceId;
import com.example.demo.services.ProjectService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class ProjectServiceImpl implements ProjectService {
	
	@Autowired
	private ProjectMapper projectMapper;

	@Autowired
	private ProcessinstanceFileMapper  processinstanceFileMapper;
	
	@Autowired
	private ProcessinstanceFileHistoryMapper processinstanceFileHistoryMapper;
	
	@Autowired
	private ProjectId_processInstanceIdMapper projectId_processInstanceIdMapper;
	
	/**获取所有项目
	 * @author  dsh
	 * @param null
	 * @return List<Project>
	 * **/
	@Override
	public List<Project> getAllProject() {
		List<Project> projectList =  projectMapper.selectAllProject();
		return projectList;
	}


	/**按条件获取所有项目
	 * @author dsh
	 * @param condtions 条件
	 * @return List<Project>
	 * **/
	@Override
	public PageInfo<Project> findProjectByConditions(Map<String, Object> condtions) {
		PageHelper.startPage((Integer)condtions.get("pageNum"), (Integer)condtions.get("pageSize")); 
		List<Project> projectList =  projectMapper.selectProjectByCondtions(condtions);
		PageInfo<Project> pf = new PageInfo<>(projectList);
		return  pf;
	}


	/**  添加项目
	 * @author dsh
	 * @param projectName 项目名称
	 * @param projectNumber 项目编号
	 * @return void
	 * **/
	@Override
	public void addProject(String projectName, String projectNumber, String platId) {
		Project project = new Project();
		project.setProjectName(projectName);
		project.setProjectNumber(projectNumber);
		project.setPlatId(platId);
		projectMapper.insertSelective(project);
	}


	/**找到项目所有相关的文件
	 * @author  dsh
	 * @param projectId 项目ID 
	 * @param status 文件状态
	 * @return List<ProcessinstanceFile>
	 * **/
	@Override
	public List<ProcessinstanceFile> findprojectProcessInstanceFileTable(String projectId, String type) {
		List<ProcessinstanceFile> processinstanceFiles = processinstanceFileMapper.selectProjectProcessInstanceFileTable(projectId, type);
		return processinstanceFiles;
	}


	/**找到文件所有相关的版本
	 * @author dsh
	 * @param projectId 项目ID 
	 * @param status 文件状态
	 * @return List<ProcessinstanceFileHistory> 
	 * **/
	@Override
	public List<Map<String, Object>> findProcessInstanceFileHistTable(String processInstanceFileId) {
		List<Map<String, Object>> processinstanceFileHistories = processinstanceFileHistoryMapper.selectByProcessInstanceFileId(processInstanceFileId);
		return processinstanceFileHistories;
	}


	@Override
	public int deleteProject(String projectId) {
		int result = projectMapper.deleteByPrimaryKey(Integer.valueOf(projectId));
		return result;
	}


	@Override
	public List<Map<String, Object>> findProcessInstanceByprojectId(String projectId, String type) {
		List<Map<String, Object>> result = projectMapper.selectProcessInstanceByProjectId(projectId, type);
		return result;
	}


	@Override
	public int findProjectIdPid(String processInstanceId) {
		ProjectId_processInstanceId projectId_processInstanceId = projectId_processInstanceIdMapper.selectByPrimaryKey(processInstanceId);
		return projectId_processInstanceId.getProjectid();
		
	}

}
