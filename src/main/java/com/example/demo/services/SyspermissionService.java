package com.example.demo.services;

import java.util.List;
import java.util.Map;

import com.example.demo.domain.Sys_permission;

public interface SyspermissionService {
	
	
	/**
	 * 根据查询条件返回权限表单
	 * @param map
	 * @return
	 */
	public List<Sys_permission> querySyspermissionTable(Map<String,String> map);
	
	
	/**
	 *       查询所有的1.2级的syspermission到前台做成select
	 * @param 
	 * @return
	 */
	public List<Sys_permission> querySyspermissionSelect();
	
	
	/**
	 *       查询所有父id为menuId的权限对象 
	 * @param map
	 * @return
	 */
	public List<Sys_permission> querySyspermissionByMenuId(Map map);
	
	
	/**
	 *       保存新建权限对象
	 * @param Sys_permission
	 * @return
	 */
	public int saveSyspermission(Sys_permission sys_permission);
	
	/**
	 *       更新权限对象
	 * @param Sys_permission
	 * @return
	 */
	public int updateSyspermission(Sys_permission sys_permission);
	
	/**
	 *       根据ID删除权限对象
	 * @param id
	 * @return
	 */
	public int deleteSyspermissionById(int id);
	
	

}
