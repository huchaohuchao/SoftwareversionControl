package com.example.demo.services;

import java.util.List;
import java.util.Map;

import org.activiti.engine.task.Attachment;

import com.example.demo.domain.ProcessinstanceFile;
import com.example.demo.domain.ProcessinstanceFileHistory;
import com.example.demo.domain.Project;
import com.github.pagehelper.PageInfo;

public interface ProjectService {
	
	/**获取所有项目
	 * @author  dsh
	 * @param null
	 * @return List<Project>
	 * **/
	public List<Project>  getAllProject();
	
	/**按条件获取所有项目
	 * @author dsh
	 * @param condtions 条件
	 * @return List<Project>
	 * **/
	public PageInfo<Project>  findProjectByConditions(Map<String,Object> condtions);
	
	/**  添加项目
	 * @author dsh
	 * @param projectName 项目名称
	 * @param projectNumber 项目编号
	 * @return void
	 * **/
	public void  addProject(String projectName, String projectNumber, String platId);
	
	
	/**找到项目所有相关的文件
	 * @author  dsh
	 * @param projectId 项目ID 
	 * @param status 文件状态
	 * @return List<ProcessinstanceFile>
	 * **/
	public List<ProcessinstanceFile>  findprojectProcessInstanceFileTable(String projectId, String type);
	
	/**找到文件所有相关的版本
	 * 
	 * @author dsh
	 * @param projectId 项目ID 
	 * @param status 文件状态
	 * @return List<ProcessinstanceFileHistory> 
	 * **/
	public List<Map<String, Object>> findProcessInstanceFileHistTable(String processInstanceFileId);
	
	
	/**删除项目Id
	 * 
	 * @author dsh
	 * @param projectId 项目ID 
	 * @return List<ProcessinstanceFileHistory> 
	 * **/
	public int deleteProject(String projectId);

	
	
	/**查找此项目相关的流程
	 * 
	 * @author dsh
	 * @param projectId 项目ID  type 流程类型
	 * @return List<Map<String,Object>>
	 * **/
	public List<Map<String,Object>> findProcessInstanceByprojectId(String projectId, String type);
	
	
	/**查找此流程所属的项目
	 * 
	 * @author dsh
	 * @param processInstanceId 流程ID
	 * @return projectId
	 * **/
	public int findProjectIdPid(String processInstanceId);
	
}
