package com.example.demo.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.example.demo.domain.Project;

@Mapper
public interface ProjectMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Project record);

    int insertSelective(Project record);

    Project selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Project record);

    int updateByPrimaryKey(Project record);
    
    //获取所有项目
    List<Project> selectAllProject();
    
    //按条件获取项目
    List<Project> selectProjectByCondtions(Map<String, Object> condtions);
    
    List<Map<String, Object>> selectProcessInstanceByProjectId(@Param("projectId")String projectId, @Param("type")String type);
   
}