package com.example.demo.listener;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import com.example.demo.dao.ProcessinstanceFileMapper;
import com.example.demo.domain.ProcessinstanceFile;
import com.example.demo.util.SpringUtils;

public class ChangeFileStatusListener implements ExecutionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void notify(DelegateExecution delegateExecution) throws Exception {
	        String process_id = delegateExecution.getProcessInstanceId();
	        ProcessinstanceFileMapper processinstanceFileMapper = (ProcessinstanceFileMapper) SpringUtils.getObject("processinstanceFileMapper");
	        List<ProcessinstanceFile> processinstanceFiles =   processinstanceFileMapper.selectProcessinstanceListFileByPid(process_id);
	        for(ProcessinstanceFile pif :processinstanceFiles) {
	        	pif.setStatus("1");
	        	String processDefinitionId = delegateExecution.getProcessDefinitionId();
	        	processDefinitionId = processDefinitionId.substring(0,processDefinitionId.indexOf(":"));
	        	pif.setType(processinstanceFileMapper.selectTypeByProcessDefinitionId(processDefinitionId)+"");
	        	processinstanceFileMapper.updateByPrimaryKeySelective(pif);
	        }
	}

}
