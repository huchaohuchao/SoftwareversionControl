package com.example.demo.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.DepartmentId_projectTypeIdMapper;
import com.example.demo.dao.DepartmentMapper;
import com.example.demo.dao.Project_typeMapper;
import com.example.demo.dao.SysroleMapper;
import com.example.demo.dao.SysuserMapper;
import com.example.demo.dao.PositionMapper;
import com.example.demo.domain.Department;
import com.example.demo.domain.Position;
import com.example.demo.domain.Project_type;
import com.example.demo.domain.Sysrole;
import com.example.demo.domain.Sysuser;
import com.example.demo.services.SysroleService;

@Service
@Transactional
public class SysroleServiceImpl implements SysroleService{
	@Autowired
	SysroleMapper sysrolemapper;
	
	@Autowired
	DepartmentMapper departmentMapper;
	
	@Autowired
	SysuserMapper sysuserMapper;	
	
	@Autowired
	Project_typeMapper project_typeMapper;
	
	@Autowired
	PositionMapper positiSonMapper;
	
	@Autowired
	DepartmentId_projectTypeIdMapper departmentId_projectTypeIdMapper;
	
	/**
	 *	角色下拉框
	 * @author hc
	 * @param 
	 * @return List<Sysrole>
	 */
	public List<Sysrole> roleSelection(){
		return sysrolemapper.roleSelection();
	}
	
	/**
	 *	部门下拉框
	 * @author hc
	 * @param 
	 * @return List<Department>
	 */
	public List<Department> departmentSelection(){
		return departmentMapper.departmentSelection();
	}

	/**
	 *	根据部门id查询部门人员
	 * @author hc
	 * @param String department_id
	 * @return List<Sysuser>
	 */
	public List<Sysuser> departmentStaffSelection(String department_id, String isNextApprovel){
		return sysuserMapper.departmentStaffSelection(department_id, isNextApprovel);
	}

	/**
	 *	获取项目类型下拉框
	 * @author hc
	 * @param 
	 * @return List<Project_type>
	 */
	public List<Project_type> projectTypeSelection(){
		return project_typeMapper.projectTypeSelection();
	}

	@Override
	public List<Position> positionSelection() {
		// TODO Auto-generated method stub
		return positiSonMapper.selectAll();
	}

	@Override
	public List<Map<String,Object>> projectTypeSelectionByDepartmentId(int departmentId) {
		List<Map<String,Object>> projectType = departmentId_projectTypeIdMapper.projectTypeSelectionByDepartmentId(Integer.valueOf(departmentId));
		return projectType;
	}
}
