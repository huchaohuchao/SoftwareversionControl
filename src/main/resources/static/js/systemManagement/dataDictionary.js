layui.use([ 'form', 'table' ], function() {
	var form = layui.form;
	var table = layui.table;
	var $ = layui.$, active = {
		reload : function() {
			var type = $('#type');
			var parent_id = $('#parent_id');
			//执行重载
			table.reload('userReload', {
				page : {
					curr : 1
				//重新从第 1 页开始
				},
				where : {
					type : type.val(),
					parent_id : parent_id.val()
				}
			});
		}
	};

	table.render({
		elem : '#sysuserTable',
		url : '../../dictionary_item/dictionaryList',
		toolbar : '#toolbarSysuser',
		title : '用户数据表',
		cols : [ [ {
			type : 'checkbox',
			fixed : 'left'
		}, {
			field : 'id',
			title : 'ID',
			width : 100,
			fixed : 'left',
			unresize : true,
			sort : true
		}, {
			field : 'sort',
			title : '序号',
			sort : true
		}, {
			field : 'value',
			title : '字典值',
			sort : true
		}, {
			field : 'type',
			title : '字典名称',
			sort : true
		}, {
			field : 'parent_id',
			title : '字典父Id',
			sort : true
		}, {
			fixed : 'right',
			title : '操作',
			toolbar : '#barSysuser',
			width : 150
		} ] ],
		id : 'userReload',
		page : true,
	});

	//头工具栏事件
	table.on('toolbar(sysuserTable)', function(obj) {
		var checkStatus = table.checkStatus(obj.config.id);
		switch (obj.event) {
		case 'getCheckData':
			var data = checkStatus.data;
			layer.alert(JSON.stringify(data));
			break;
		case 'getCheckLength':
			var data = checkStatus.data;
			layer.msg('选中了：' + data.length + ' 个');
			break;
		case 'isAll':
			layer.msg(checkStatus.isAll ? '全选' : '未全选');
			break;
		};
	});

	//监听行工具事件
	table.on('tool(sysuserTable)', function(obj) {
		var data = obj.data;
		//console.log(obj)
		if (obj.event === 'del') {
			layer.confirm('真的删除数据字典吗么', function(index) {
				obj.del();
				dictionaryDelete(data.id);
				layer.close(index);
			});
		} else if (obj.event === 'edit') {
			/* 再弹出编辑界面 */
			layer.open({
				type : 1,
				title : "编辑数据字典",
				skin : "myclass",
				area : [ "40%" ],
				btn : [ '保存', '取消' ],
				content : $("#dictionaryAdd"),
				success : function(layero) {
					$('#dictionarySort').val(data.sort);
					$('#dictionaryValue').val(data.value);
					$('#dictionaryType').val(data.type);
					$('#text').val(data.text);
					$('#parentId').val(data.parent_id);
					var btn = layero.find('.layui-layer-btn');
					btn.css('text-align', 'center');
					/* 渲染表单 */
					form.render();
				},
				yes : function(index, layero) {
					var dictionarySort = $('#dictionarySort').val();
					var dictionaryValue = $('#dictionaryValue').val();
					var dictionaryType = $('#dictionaryType').val();
					var text = $('#text').val();
					var parentId = $('#parentId').val();
					$.ajax({
						type : "POST",//方法类型
						dataType : "json",//预期服务器返回的数据类型
						url : "../../dictionary_item/dictionaryAdd",//url
						data : {
							sort : dictionarySort,
							value : dictionaryValue,
							type : dictionaryType,
							text : text,
							parent_id : parentId,
							id : data.id,
							},
						success : function(result) {
							if (result.code == 0) {
								layer.close(index);
								window.location.reload();
							};
						},
						error : function() {
							layer.msg('编辑用户失败');
							layer.close(index);
						}
					});
				}
			});
		}
	});

	$('.userTable .layui-btn').on('click', function() {
		var type = $(this).data('type');
		active[type] ? active[type].call(this) : '';
	});

	function dictionaryDelete(id) {
		$.ajax({
			url : '../../dictionary_item/dictionaryDelete?id=' + id,
			contentType : "application/json;charset=utf-8",
			type : 'post',
			success : function(data) {
				var json = JSON.parse(data);
				if (json.code == '0') {
					layer.msg('删除数据字典成功');
				} else {
					layer.msg('删除数据字典失败');
				}
			}
		})
	}

	/* 点击添加按钮提出添加员工界面 */
	$("#addEmplpyeeBtn").click(function() {
		/* 再弹出添加界面 */
		layer.open({
			type : 1,
			title : "添加数据字典",
			skin : "myclass",
			area : [ "40%" ],
			btn : [ '保存', '取消' ],
			content : $("#dictionaryAdd"),
			success : function(layero) {
				var btn = layero.find('.layui-layer-btn');
				btn.css('text-align', 'center');
				/* 渲染表单 */
				form.render();
				document.getElementById("addEmployeeForm").reset();//清空form表单的值 
			},
			yes : function(index, layero) {
				//限制输入框中不能用空值
			     var num=0;
			     $("addEmployeeForm input[type$='text']").each(function(n){
			    	 if($(this).val()==""){
				    	num++
			    	 }		         
			     });
			     if(num>0){
			    	 layer.msg('输入框不能为空');
			    	 return false;
			     }
				var dictionarySort = $('#dictionarySort').val();
				var dictionaryValue = $('#dictionaryValue').val();
				var dictionaryType = $('#dictionaryType').val();
				var text = $('#text').val();
				var parentId = $('#parentId').val();
				$.ajax({
					type : "POST",//方法类型
					dataType : "json",//预期服务器返回的数据类型
					url : "../../dictionary_item/dictionaryAdd",//url
					data : {
						sort : dictionarySort,
						value : dictionaryValue,
						type : dictionaryType,
						text : text,
						parent_id : parentId
					},
					success : function(result) {
						if (result.code == 0) {
							layer.close(index);
							layer.msg('新增数据字典成功');
						}
						;
					},
					error : function() {
						layer.msg('新增数据字典失败');
						layer.close(index);
					}
				});
			}
		});
	});
});