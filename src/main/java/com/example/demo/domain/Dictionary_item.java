package com.example.demo.domain;

public class Dictionary_item {

	private Integer id;

    private Integer sort;

    private String value;

    private String type;

    private String text;
    
    private Integer parent_id;

    public Integer getParent_id() {
		return parent_id;
	}

	public void setParent_id(Integer parent_id) {
		this.parent_id = parent_id;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value == null ? null : value.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

	@Override
	public String toString() {
		return "Dictionary_item [id=" + id + ", sort=" + sort + ", value=" + value + ", type=" + type + ", text=" + text
				+ ", parent_id=" + parent_id + "]";
	}
    
}